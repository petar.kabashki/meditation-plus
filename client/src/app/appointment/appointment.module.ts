import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared';
import { ProfileModule } from '../profile';
import { AppointmentComponent } from './appointment.component';
import { AppointmentEffect } from './effects/appointment.effects';
import { EffectsModule } from '@ngrx/effects';

const routes: Routes = [
  { path: '', component: AppointmentComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ProfileModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([
      AppointmentEffect
    ])
  ],
  declarations: [
    AppointmentComponent
  ],
  exports: [
    AppointmentComponent
  ]
})
export class AppointmentModule { }
