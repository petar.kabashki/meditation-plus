import * as _ from 'lodash';
import * as appointment from '../actions/appointment.actions';
import { AppState } from '../../reducers';
import { createSelector } from '@ngrx/store';
import { Meeting } from '../appointment';

export interface AppointmentState {
  meeting: Meeting;
  initiateSuccess: boolean;
  initiatePending: boolean;
  initiateError: string;
}

export const initialAppointmentState: AppointmentState = {
  meeting: null,
  initiateSuccess: false,
  initiatePending: false,
  initiateError: ''
};


export function appointmentReducer(
  state = initialAppointmentState,
  action: appointment.Actions
): AppointmentState {
  switch (action.type) {
    case appointment.LOAD_MEETING_DONE: {
      return {
        ..._.cloneDeep(state),
        meeting: action.payload
      };
    }
    case appointment.INITIATE_MEETING: {
      return {
        ..._.cloneDeep(state),
        initiateSuccess: false,
        initiatePending: true,
        initiateError: ''
      };
    }
    case appointment.INITIATE_MEETING_DONE: {
      return {
        ..._.cloneDeep(state),
        initiateSuccess: true,
        initiatePending: false,
        initiateError: ''
      };
    }
    case appointment.INITIATE_MEETING_ERROR: {
      return {
        ..._.cloneDeep(state),
        initiateSuccess: false,
        initiatePending: false,
        initiateError: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

// Selectors for easy access
export const selectAppointment = (state: AppState) => state.appointment;
export const selectMeeting = createSelector(selectAppointment, (state: AppointmentState) => state.meeting);
export const selectMeetingInitiateSuccess = createSelector(selectAppointment, (state: AppointmentState) => state.initiateSuccess);
export const selectMeetingInitiatePending = createSelector(selectAppointment, (state: AppointmentState) => state.initiatePending);
export const selectMeetingInitiateError = createSelector(selectAppointment, (state: AppointmentState) => state.initiateError);
