import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MessageService } from 'app/message/message.service';
import { concatMap, map } from 'rxjs/operators';
import { SyncMessages, SYNC, SyncMessagesDone } from '../actions/message.actions';
import { of } from 'rxjs';
import * as moment from 'moment';

@Injectable()
export class SyncMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MessageService,
  ) {
  }

  @Effect()
  sync$ = this.actions$
    .ofType<SyncMessages>(SYNC)
    .pipe(
      map(action => action.payload),
      concatMap(payload =>
        this.service.synchronize(moment(payload.from), payload.to).pipe(
          concatMap(messages => of(new SyncMessagesDone({ index: payload.index, messages })))
        )
      )
    );
}
