import request from 'superagent';
import config from '../../config/config.json';

const discord = {

  /**
   * Creates a temporary invite link for the discord
   * appointments server. The link satisfies the following
   * requirements:
   *  - Can be used only within the next 30 minutes
   *  - Can be used only 15 times
   *  - Only grants temporary membership
   */
  createAppointmentInviteLink: async () => {
    if (process.env.NODE_ENV === 'test') {
      return 'testing';
    }

    const apiResponse = await request
      .post(`https://discordapp.com/api/v6/channels/${config.DISCORD_CHANNEL_ID}/invites`)
      .set({
        'Authorization': `Bot ${config.DISCORD_BOT_TOKEN}`,
        'User-Agent': 'DiscordBot (https://meditation.sirimangalo.org, v0.1)',
        'Content-Type': 'application/json'
      })
      .retry(3)
      .send({
        max_age: 1800, // 30 minutes
        max_uses: 15,
        temporary: true,
        unique: true
      });

    if (apiResponse.error && (!apiResponse.body.code || apiResponse.body.code.length <= 0)) {
      throw apiResponse.error;
    }

    return `https://discord.gg/${apiResponse.body.code}`;
  }
};

export { discord };
