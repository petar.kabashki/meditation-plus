import { visiblePublicFields, User } from '../models/user.model.js';

export const getUser = async (query) =>  {
  let doc = await User
    .findOne(query)
    .lean()
    .exec();

  if (!doc) return null;

  const user = { local: {} };
  if (doc.showEmail) {
    user.local.email = doc.local.email;
  }

  visiblePublicFields
    .filter(key => doc.hasOwnProperty(key))
    .forEach(key => user[key] = doc[key]);

  user.role = doc.role || 'ROLE_USER';

  return user;
};
